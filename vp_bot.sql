-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 20, 2020 at 09:08 PM
-- Server version: 10.3.24-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vp_bot`
--

-- --------------------------------------------------------

--
-- Table structure for table `vp_pm_data`
--

CREATE TABLE `vp_pm_data` (
  `kode_data_vp` varchar(9) NOT NULL,
  `kode_user` varchar(9) NOT NULL,
  `isi_vp` varchar(300) NOT NULL,
  `tanggal_vp` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vp_pm_member`
--

CREATE TABLE `vp_pm_member` (
  `kode_user` varchar(9) NOT NULL,
  `kode_chat_user` varchar(20) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Non-Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vp_pm_data`
--
ALTER TABLE `vp_pm_data`
  ADD PRIMARY KEY (`kode_data_vp`),
  ADD KEY `kode_user` (`kode_user`);

--
-- Indexes for table `vp_pm_member`
--
ALTER TABLE `vp_pm_member`
  ADD PRIMARY KEY (`kode_user`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `vp_pm_data`
--
ALTER TABLE `vp_pm_data`
  ADD CONSTRAINT `vp_pm_data_ibfk_1` FOREIGN KEY (`kode_user`) REFERENCES `vp_pm_member` (`kode_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
