<?php
    include "config/koneksi.php";

	# KODE DATA
	$nama_tabel        = "vp_pm_data";
	$kd_primary        = "kode_data_vp";
	$kd_text           = "PMV";
	$kode_data_vp      = KODE($nama_tabel, $kd_primary, $kd_text);
        
    # KODE USER
	$nama_tabel        = "vp_pm_member";
	$kd_primary        = "kode_user";
	$kd_text           = "MEM";
	$kode_user         = KODE($nama_tabel, $kd_primary, $kd_text);
	
	# EMOJI https://apps.timwhitlock.info/emoji/tables/unicode
	$senyum  = "\xF0\x9F\x98\x84";
	$senyum2 = "\xF0\x9F\x98\x81";
	$ngakak  = "\xF0\x9F\x98\x82";
	$ganteng = "\xF0\x9F\x98\x8E";
	$centang = "\xE2\x9C\x85";
	$rocket  = "\xF0\x9F\x9A\x80";
	$fire    = "\xF0\x9F\x94\xA5";
	$hehe    = "\xF0\x9F\x99\x88";
	$love    = "\xE2\x9D\xA4";
	$warning = "\xE2\x9A\xA0";
	$diamond = "\xF0\x9F\x92\xA0";
	$clan    = "\xF0\x9F\x94\xB0";
	
    $token_bot  = "XXXXXXXX";
    $path_api   = "https://api.telegram.org/bot".$token_bot;
    $update     = json_decode(file_get_contents("php://input"), TRUE);
    
    $chat_id     = $update["message"]["chat"]["id"];
    $nama_user   = $update["message"]["chat"]["first_name"]." ".$update["message"]["chat"]["last_name"];
    $pesan       = $update["message"]["text"];
    
    # KIRIM PESAN BOT KE USER
    function kirim_pesan($chat_id, $isi){
        $isi   = urlencode($isi);
        $hasil = file_get_contents($GLOBALS['path_api']."/sendmessage?chat_id=".$chat_id."&text=".$isi."&parse_mode=html");
        return $hasil;
    }
    
    function left($str, $length) {
         return substr($str, 0, $length);
    }

    # GET DATA VP USER
    function get_data_vp($chat_id, $tanggal_vp){
        $warning = $GLOBALS['warning'];
        $sql = mysqli_query($GLOBALS['KONEKSI'], "SELECT * FROM vp_pm_data
        LEFT JOIN vp_pm_member ON (vp_pm_data.kode_user = vp_pm_member.kode_user)
        WHERE vp_pm_member.kode_chat_user='".$chat_id."' AND vp_pm_data.tanggal_vp='".$tanggal_vp."'");
        $data = mysqli_fetch_array($sql);
        if($data){
            $hasil = $data['isi_vp'];
        }else{
            $hasil = $warning." Belum ada ".$warning;
        }
        
        return $hasil;
    }
    
    # ID CHAT TELE MEMBER PM
    $id_gery     = "XXXXXXXX"; 
    $id_diaulhak = "XXXXXXXX";
    $id_adit     = "XXXXXXXX";
    $id_syawal   = "XXXXXXXX";
    $id_fujianto = "XXXXXXXX";
    $id_khairul  = "XXXXXXXX";
    $id_fauji    = "XXXXXXXX";
    
    # GET TELEGRAM DATA
    $q_cek = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_member WHERE kode_chat_user='".$chat_id."'");
    $dt_cek = mysqli_fetch_array($q_cek);
    if(!$dt_cek && !empty($chat_id)){
        $sql2	= mysqli_query($KONEKSI, "INSERT INTO vp_pm_member(
                kode_user, 
                kode_chat_user,
                nama_user
               ) 
                VALUES(
                '$kode_user',
                '$chat_id',
                '$nama_user'
                )")or die('Error: '.mysqli_error($KONEKSI));
        kirim_pesan($id_fujianto, $fire." Info : Ada Member Baru ".$fire."\n\nNama : ".$nama_user);
    }  
    
    # CASE HARI SENIN, KEMARIN HARI JUM'AT
    $hari_case = date('D', strtotime($tanggal_sekarang));
    if($hari_case == "Mon"){
        $tanggal_kemarin = date("Y-m-d", strtotime($tanggal_sekarang." - 3 day"));
    }
    
    # DATA VP HARI KEMARIN
    $gery_yesterday     = get_data_vp($id_gery, $tanggal_kemarin);
    $diaulhak_yesterday = get_data_vp($id_diaulhak, $tanggal_kemarin);
    $adit_yesterday     = get_data_vp($id_adit, $tanggal_kemarin);
    $syawal_yesterday   = get_data_vp($id_syawal, $tanggal_kemarin);
    $fujianto_yesterday = get_data_vp($id_fujianto, $tanggal_kemarin);
    $khairul_yesterday  = get_data_vp($id_khairul, $tanggal_kemarin);
    $fauji_yesterday    = get_data_vp($id_fauji, $tanggal_kemarin);

    # DATA VP HARI INI
    $gery_today     = get_data_vp($id_gery, $tanggal_sekarang);
    $diaulhak_today = get_data_vp($id_diaulhak, $tanggal_sekarang);
    $adit_today     = get_data_vp($id_adit, $tanggal_sekarang);
    $syawal_today   = get_data_vp($id_syawal, $tanggal_sekarang);
    $fujianto_today = get_data_vp($id_fujianto, $tanggal_sekarang);
    $khairul_today  = get_data_vp($id_khairul, $tanggal_sekarang);
    $fauji_today    = get_data_vp($id_fauji, $tanggal_sekarang);
    
    # [TESTING]
    function get_data_test($tanggal_vp){
        GLOBAL $id_gery, $id_diaulhak, $id_adit, $id_syawal, $id_fujianto, $id_khairul, $id_fauji;
        $sql = mysqli_query($GLOBALS['KONEKSI'], "SELECT * FROM vp_pm_data
        LEFT JOIN vp_pm_member ON (vp_pm_data.kode_user = vp_pm_member.kode_user)
        WHERE vp_pm_member.kode_chat_user NOT IN(
        '".$id_gery."', 
        '".$id_diaulhak."', 
        '".$id_adit."',  
        '".$id_syawal."',  
        '".$id_fujianto."',  
        '".$id_khairul."',  
        '".$id_fauji."') AND vp_pm_data.tanggal_vp='".$tanggal_vp."' ORDER BY vp_pm_data.kode_data_vp ASC");
        $i= 8;
        while($dt_vp_test = mysqli_fetch_array($sql)){
            $hasil .= "\n<b>".$i.". ".$dt_vp_test['nama_user']." :</b> ".$dt_vp_test['isi_vp']."\n";
            $i++;
        }
        if(empty($hasil)){
            $hasil = "\nTidak ada data.";
        }
        return $hasil;
    }
    
    # CASE TEST 
    if($chat_id == $id_gery ||
       $chat_id == $id_diaulhak ||
       $chat_id == $id_adit ||
       $chat_id == $id_syawal ||
       $chat_id == $id_fujianto ||
       $chat_id == $id_khairul ||
       $chat_id == $id_fauji
    ){
        $pm = true;
    }else{
        $pm = false;
        $test_kemarin  = get_data_test($tanggal_kemarin);
        $test_hari_ini = get_data_test($tanggal_sekarang);
        
        if($test_kemarin == "\nTidak ada data."){
            $test_kemarin  = "";
        }else{
            $test_kemarin  = "\n\n".$clan." <b>Lainnya [Testing]: ".$clan."</b>".get_data_test($tanggal_kemarin);
        }
        
        if($test_hari_ini == "\nTidak ada data."){
            $test_hari_ini = "";
        }else{
            $test_hari_ini = "\n\n".$clan." <b>Lainnya [Testing]: ".$clan."</b>".get_data_test($tanggal_sekarang);
        }
    }

    # CASE PESAN
    $pesan_length = strlen($pesan);
    
    $pesan_add = left($pesan, 5);
    $pesan_add_isi = str_replace($pesan_add, "", $pesan);
    $pesan_add_isi = str_replace("<","&#60;",$pesan_add_isi);
    $pesan_add_isi = str_replace(">","&#62;",$pesan_add_isi);
    
    $pesan_edit = left($pesan, 6);
    $pesan_edit_isi = str_replace($pesan_edit, "", $pesan);
    $pesan_edit_isi = str_replace("<","&#60;",$pesan_edit_isi);
    $pesan_edit_isi = str_replace(">","&#62;",$pesan_edit_isi);
    
    # CASE SECURITY
    $KODE_ACT = "12345678";
    $pesan_act = left($pesan, 5);
    $pesan_act_isi = str_replace($pesan_act, "", $pesan);
    $sql_mem = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_member WHERE kode_chat_user='".$chat_id."' AND status='Aktif'");
    $dt_mem = mysqli_fetch_array($sql_mem);
    
    # CASE CRONJOB
    $go_cronjob = $argv[1];
    
    # CASE PERINTAH BOT
    if($pesan == "/start"){
        kirim_pesan($chat_id, "Selamat Datang di <b>VP PM BOT</b> ".$senyum."\n\nBerikut Perintah Untuk Menjalankan Bot\n\n/vp_hari_ini - Tampilkan VP Hari Ini\n\n/vp_kemarin - Tampilkan VP Kemarin\n\n/add - Tambah VP Hari Ini\n\n/lanjut - Lanjutkan VP Kemarin\n\n/edit - Mengedit VP\n\n<b>Note : </b>Untuk menjalankan perintah bisa langsung klik perintah diatas ");
    }else if(!$dt_mem && !empty($chat_id)){
        if($pesan == "/act" || $pesan_act == "/act "){
            if($pesan_length > 5){
                if($KODE_ACT == $pesan_act_isi){
                    $sql  =  mysqli_query($KONEKSI, "update vp_pm_member set 
                    status='Aktif'
                    where kode_chat_user='$chat_id'")or die('Error: '.mysqli_error($KONEKSI));
                    if($sql){
                        kirim_pesan($chat_id, "Selamat aktivasi berhasil ".$centang."\nsekarang semua fitur dapat digunakan ".$senyum);
                    } 
                }else{
                    kirim_pesan($chat_id, $warning." Maaf, kode aktivasi tidak valid.");
                }
            }else{
                kirim_pesan($chat_id, "<b>Format : </b> /act [8 digit kode]\n\n<b>Contoh : </b>/act 12345678");
            }
        }else{
            kirim_pesan($chat_id, "Untuk Menggunakan Fitur Bot silahkan minta kode aktivasi ke @fujianto21 ".$senyum."\n\n/act - Untuk Aktivasi");
        }
    }else if($pesan == "/act" || $pesan_act == "/act "){
        kirim_pesan($chat_id, "Akun sudah aktif ".$centang."\nTidak perlu aktivasi kembali ".$hehe);
    }else if($hari_case == "Sat" || $hari_case == "Sun"){
        kirim_pesan($chat_id, "Libur Boss ".$ngakak);
    }else{
        if($pesan == "/vp_kemarin"){
            kirim_pesan($chat_id, $rocket." <b>List VP Kemarin :</b> ".$rocket."\n\nTanggal : ".NamaHari($tanggal_kemarin).", ".Tanggal_ID($tanggal_kemarin)."\n\n<b>1. Grahito Gery N. :</b> ".$gery_yesterday."\n\n<b>2. Diaulhak :</b> ".$diaulhak_yesterday."\n\n<b>3. Adityo E. Wibowo :</b> ".$adit_yesterday."\n\n<b>4. Mochamad Syawalu R. :</b> ".$syawal_yesterday."\n\n<b>5. Fujianto :</b> ".$fujianto_yesterday."\n\n<b>6. Khairul Akhyar :</b> ".$khairul_yesterday."\n\n<b>7. M. Fauji :</b> ".$fauji_yesterday." ".$test_kemarin);
        }else if($pesan == "/vp_hari_ini"){
            kirim_pesan($chat_id, $fire." <b>List VP Hari Ini :</b> ".$fire."\n\nTanggal : ".NamaHari($tanggal_sekarang).", ".Tanggal_ID($tanggal_sekarang)."\n\n<b>1. Grahito Gery N. :</b> ".$gery_today."\n\n<b>2. Diaulhak :</b> ".$diaulhak_today."\n\n<b>3. Adityo E. Wibowo :</b> ".$adit_today."\n\n<b>4. Mochamad Syawalu R. :</b> ".$syawal_today."\n\n<b>5. Fujianto :</b> ".$fujianto_today."\n\n<b>6. Khairul Akhyar :</b> ".$khairul_today."\n\n<b>7. M. Fauji :</b> ".$fauji_today." ".$test_hari_ini);
        }else if($pesan == "/lanjut"){
            $q_cek = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_data
            LEFT JOIN vp_pm_member ON (vp_pm_data.kode_user = vp_pm_member.kode_user)
            WHERE vp_pm_member.kode_chat_user='".$chat_id."' AND vp_pm_data.tanggal_vp='".$tanggal_kemarin."'");
            $dt_cek = mysqli_fetch_array($q_cek);
            $isi_vp = $dt_cek['isi_vp'];
            
            $q_cek2 = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_data
            LEFT JOIN vp_pm_member ON (vp_pm_data.kode_user = vp_pm_member.kode_user)
            WHERE vp_pm_member.kode_chat_user='".$chat_id."' AND vp_pm_data.tanggal_vp='".$tanggal_sekarang."'");
            $dt_cek2 = mysqli_fetch_array($q_cek2);
            
            if($dt_cek2){
                kirim_pesan($chat_id, "VP Hari ini sudah ada ".$hehe."\n /edit vp jika ingin mengubahnya");  
            }else{
                if(!$dt_cek){
                    kirim_pesan($chat_id, "VP kemarin tidak ada ".$hehe."\n /add vp untuk menambahkan vp hari ini");
                }else{
                        $q_mem = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_member WHERE kode_chat_user='".$chat_id."'");
                        $dt_mem = mysqli_fetch_array($q_mem);
                        $kode_user = $dt_mem['kode_user'];

                        $sql = mysqli_query($KONEKSI, "INSERT INTO vp_pm_data( 
                        kode_data_vp,
                        kode_user,
                        isi_vp,
                        tanggal_vp
                       ) 
                        VALUES(
                        '$kode_data_vp',
                        '$kode_user',
                        '$isi_vp',
                        '$tanggal_sekarang'
                        )")or die('Error: '.mysqli_error($KONEKSI));    
                    if($sql){
                         kirim_pesan($chat_id, "Oke Thx ".$senyum."\n\n<b>VP yang dilanjut :</b> ".$isi_vp);
                    }
                }    
            }
            
        }else if($pesan == "/add" || $pesan_add == "/add "){
            $q_cek = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_data
            LEFT JOIN vp_pm_member ON (vp_pm_data.kode_user = vp_pm_member.kode_user)
            WHERE vp_pm_member.kode_chat_user='".$chat_id."' AND vp_pm_data.tanggal_vp='".$tanggal_sekarang."'");
            $dt_cek = mysqli_fetch_array($q_cek);
            if($dt_cek){
                kirim_pesan($chat_id, "VP Hari ini sudah ada ".$hehe."\n /edit vp jika ingin mengubahnya");
            }else if($pesan_length > 4){
                $q_mem = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_member WHERE kode_chat_user='".$chat_id."'");
                $dt_mem = mysqli_fetch_array($q_mem);
                $kode_user = $dt_mem['kode_user'];

                $sql = mysqli_query($KONEKSI, "INSERT INTO vp_pm_data( 
                kode_data_vp,
                kode_user,
                isi_vp,
                tanggal_vp
                ) 
                VALUES(
                '$kode_data_vp',
                '$kode_user',
                '$pesan_add_isi',
                '$tanggal_sekarang'
                )")or die('Error: '.mysqli_error($KONEKSI));

                if($sql){
                    kirim_pesan($chat_id, "Oke disimpan ".$centang);
                }
            }else{
                kirim_pesan($chat_id, "Oke, silahkan ketik vp nya..\n<b>Format :</b> /add text vpnya");
            }
        }else if($pesan == "/edit" || $pesan_edit == "/edit "){
            $q_cek = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_data
            LEFT JOIN vp_pm_member ON (vp_pm_data.kode_user = vp_pm_member.kode_user)
            WHERE vp_pm_member.kode_chat_user='".$chat_id."' AND vp_pm_data.tanggal_vp='".$tanggal_sekarang."'");
            $dt_cek = mysqli_fetch_array($q_cek);

            if(!$dt_cek){
                kirim_pesan($chat_id, "VP Hari ini belum ada ".$hehe."\n /add vp untuk menambahkan vp hari ini");
            }else if($pesan_length > 5){
                $q_mem = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_member WHERE kode_chat_user='".$chat_id."'");
                $dt_mem = mysqli_fetch_array($q_mem);
                $kode_user = $dt_mem['kode_user'];

                $sql  =  mysqli_query($KONEKSI, "update vp_pm_data set 
                isi_vp='$pesan_edit_isi'
                where kode_user='$kode_user' AND tanggal_vp='".$tanggal_sekarang."'")or die('Error: '.mysqli_error($KONEKSI));
                if($sql){
                    kirim_pesan($chat_id, "Oke disimpan ".$centang);
                }
            }else{
                kirim_pesan($chat_id, "Oke, silahkan diedit vp nya.. ".$senyum."\n<b>Format :</b> /edit text vpnya\n\n<b>VP Sekarang :</b>\n");
                kirim_pesan($chat_id, "/edit ".$dt_cek['isi_vp']);
            }
        }else if($go_cronjob == "notif-vp" || ($pesan == "/notif_vp" && $chat_id == $id_fujianto)){
            $total_notif = 0;
            $q_cek = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_member WHERE status='Aktif'");
            while($dt_cek = mysqli_fetch_array($q_cek)){
                $kode_user = $dt_cek['kode_user'];
                $chat_id = $dt_cek['kode_chat_user'];
                $sql = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_data WHERE kode_user='".$kode_user."' AND tanggal_vp='".$tanggal_sekarang."'");
                $dt_vp = mysqli_fetch_array($sql);
                
                $sql_2 = mysqli_query($KONEKSI, "SELECT * FROM vp_pm_data WHERE kode_user='".$kode_user."' AND tanggal_vp='".$tanggal_kemarin."'");
                $dt_vp_kemarin = mysqli_fetch_array($sql_2);
                if($dt_vp_kemarin){ $vp_kemarin = $dt_vp_kemarin['isi_vp']; }else{ $vp_kemarin = "Tidak ada"; }
                
                if(!$dt_vp){
                    kirim_pesan($chat_id, UcapanSelamat().", VP hari ini apa gaes ".$senyum."\n\n<b>VP Kemarin : </b>".$vp_kemarin."\n\nBerikut Perintah Untuk Menjalankan Bot\n\n/vp_hari_ini - Tampilkan VP Hari Ini\n\n/vp_kemarin - Tampilkan VP Kemarin\n\n/add - Tambah VP Hari Ini\n\n/lanjut - Lanjutkan VP Kemarin\n\n/edit - Mengedit VP");
                    $total_notif++;
                }
            }
            if($total_notif > 0){
                kirim_pesan($id_fujianto, "Notif VP berhasil dikirim ".$centang);
            }
        }else{
            kirim_pesan($chat_id, "Format Pesan Tidak Valid Gaes! ".$hehe."\n\nMade with ".$love." by @fujianto21");
        }
    }
?>
