<?php
/*
	my_function.php
	Made with love by @fujianto21
*/

# FUNCTION REDIRECT URL
function redirect($url){
	if(!headers_sent()){
		header('Location: '.$url);
	}else{
		echo '<script type="text/javascript">';
		echo 'window.location.href="'.$url.'";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
		echo '</noscript>';
	}
	exit;
}

# FUNCTION FLIPDATE
function FlipDate($type, $s){
	switch($type){
	  case 'dmy':
		list($y,$m,$d) = explode('-',$s);
		return $d.'-'.$m.'-'.$y;
	  break;
	  case 'ymd':
		list($d,$m,$y) = explode('-',$s);
		return $y.'-'.$m.'-'.$d;
	  break;
	}
	return false;
}
# FUNCTION FLIPDATE BULAN DAN TAHUN ATAU HARI DAN BULAN
function FlipDate_2($type, $s){
	switch($type){
	  case 'my':
		list($y,$m,$d) = explode('-',$s);
		return $m.'-'.$y;
	  break;
	  case 'dm':
		list($d,$m,$y) = explode('-',$s);
		return $d.'-'.$m;
	  break;
	}
	return false;
}

# FUNCTION SECURITY QUERY
function db_es($t){
    GLOBAL $KONEKSI;
	return mysqli_real_escape_string($KONEKSI, $t);
}

# FUNCTION NAMA HARI
function NamaHari($tanggal){
	$hari_case = date('D', strtotime($tanggal)); 
	switch($hari_case){
	 case 'Sun':
		$getHari = "Minggu";
	 break;
	 case 'Mon': 
		$getHari = "Senin";
	 break;
	 case 'Tue':
		$getHari = "Selasa";
	 break;
	 case 'Wed':
		$getHari = "Rabu";
	 break;
	 case 'Thu':
		$getHari = "Kamis";
	 break;
	 case 'Fri':
		$getHari = "Jum'at";
	 break;
	 case 'Sat':
		$getHari = "Sabtu";
	 break;
	 default:
		$getHari = "Salah"; 
	 break;
	}
	
	return $getHari;
	 }

# FUNCTION NAMA BULAN
function NamaBulan($tanggal){
	
	$bulan_case = date("F", strtotime(FlipDate('ymd', $tanggal)));
	switch($bulan_case){
	 case 'January':
		$getbulan = "Januari";
	 break;
	 case 'February': 
		$getbulan = "Februari";
	 break;
	 case 'March':
		$getbulan = "Maret";
	 break;
	 case 'April':
		$getbulan = "April";
	 break;
	 case 'May':
		$getbulan = "Mei";
	 break;
	 case 'June':
		$getbulan = "Juni";
	 break;
	 case 'July':
		$getbulan = "Juli";
	 break;
	 case 'August':
		$getbulan = "Agustus";
	 break;
	 case 'September':
		$getbulan = "September";
	 break;
	 case 'October':
		$getbulan = "Oktober";
	 break;
	 case 'November':
		$getbulan = "November";
	 break;
	 case 'December':
		$getbulan = "Desember";
	 break;
	 default:
		$getbulan = "Salah"; 
	 break;
	}
	
	return $getbulan;
}

# FUNCTION TANGGAL INDONESIA
function Tanggal_ID($tanggal){
	$hari = date("d", strtotime(FlipDate('ymd', $tanggal)));
	$bulan = NamaBulan($tanggal);
	$tahun = date("Y", strtotime(FlipDate('ymd', $tanggal)));

	$tanggal_id = $hari.' '.$bulan.' '.$tahun;
	return $tanggal_id;
}

# FUNCTION UCAPAN SELAMAT
function UcapanSelamat(){ 
	 $b = time();
	 $hour = date("G",$b);
	 
	 if ($hour>=0 && $hour<=11){
		return "Selamat Pagi";
	 }elseif ($hour >=12 && $hour<=14){
			return "Selamat Siang";
	 }elseif ($hour >=15 && $hour<=17){
			return "Selamat Sore";
	 }elseif ($hour >=17 && $hour<=18){
			return "Selamat Petang";
	 }elseif ($hour >=19 && $hour<=23){
			return "Selamat Malam";
	 }
}

# FUNCTION KODE 3 HURUF & 6 ANGKA
function KODE($nama_tabel, $kd_primary, $kd_text){
		# I LOVE MY KODE
        GLOBAL $KONEKSI;
		# UNTUK QUERY KODE 
		$query = "SELECT CONCAT('".$kd_text."',max(".$kd_primary.")) as maxkode FROM (SELECT ABS(substr(".$kd_primary.",4,9)) as ".$kd_primary." FROM ".$nama_tabel.") A";
		$hasil = mysqli_query($KONEKSI, $query);
		$data  = mysqli_fetch_array($hasil);
		$kd_hasil_max = $data['maxkode'];
		$noUrut = (int) substr($kd_hasil_max,3,9);
		$noUrut++;
		# JIKA NO URUT 1 - 99
		if($noUrut >= 1 && $noUrut <= 99){
				$char_case1 = $kd_text."0000";
				$kd_hasil = $char_case1 . sprintf("%02s", $noUrut);
		# JIKA NO URUT 100 - 999
		}else if($noUrut >= 100 && $noUrut <= 999){
				$char_case2 = $kd_text."000";
				$kd_hasil = $char_case2 . sprintf("%02s", $noUrut);
		# JIKA NO URUT 1000 - 9999
		}else if($noUrut >= 1000 && $noUrut <= 9999){
				$char_case3 = $kd_text."00";
				$kd_hasil = $char_case3 . sprintf("%02s", $noUrut);
		# JIKA NO URUT 10000 - 99999
		}else if($noUrut >= 10000 && $noUrut <= 99999){
				$char_case4 = $kd_text."0";
				$kd_hasil = $char_case4 . sprintf("%02s", $noUrut);
		# JIKA NO URUT 100000 - 99999
		}else if($noUrut >= 100000 && $noUrut <= 999999){
				$char_case5 = $kd_text;
				$kd_hasil = $char_case5 . sprintf("%02s", $noUrut);
		}
		return $kd_hasil;
}
?>
