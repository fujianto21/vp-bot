<?php
require_once "app_config.php";

$KONEKSI = mysqli_connect($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_DATABASE);
if (mysqli_connect_errno()){
  $error_db = '
        <style>
          .info_db{
          background-color:#ff0000;
          color:#ffffff;
          padding:10px;
          text-align:center;
          margin:5 0 5 0;
          font-size:20px;
          border-radius: 5px;
          font-family:calibri;
          }
      </style>
   <div class="info_db">
     <b>
     <span style="font-size:50px">'.mysqli_connect_error().'</span><br>
     <i>For more information, please call 0897-3219-123.</i>
     </b>
   </div>';
   die($error_db);
}

session_start();
require_once "my_function.php";
?>
