<?php
# CONFIG DATABASE
$DB_HOSTNAME = "localhost";
$DB_USER	 = "root";
$DB_PASSWORD = "";
$DB_DATABASE = "vp_bot";

# CONFIG APLIKASI
$BASE_URL = "https://fujianto21.com/app/bot";
$NAMA_APLIKASI = "BOT VP";
$domain_utama = "https://fujianto21.com";

# WAKTU
date_default_timezone_set('Asia/Jakarta');
$tanggal_sekarang = date("Y-m-d");
$tanggal_kemarin  = date("Y-m-d", strtotime("- 1 day"));
$waktu_sekarang   = date("H:i:s");
$bulan_sekarang   = date("m");
$tahun_sekarang   = date("Y");
?>
